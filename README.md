# 2pend
This is a program that simulates a double pendulum and renders it on the console.
Press `q` or Escape to quit and `r` to randomize.

![Preview](https://bitbucket.org/spilt/2pend/downloads/2pend.gif)

## Building
`make` to build and `make install` to install.
